# 11ty_silex.me 2024

## Local dev

Clone the repo and its submodules:

```bash
$ git clone --recurse-submodules
```

Install dependencies:

```bash
npm install
```

Start Silex + 11ty dev server:

```bash
npm start
```

Then open Silex at http://localhost:6805

And open http://localhost:8080 for the preview

When you publish in Silex, the preview will be updated automatically.

Contribution: commit the changes in the submodule `default`, push and create a merge request.

## Deploy

```bash
npm version patch && git push && git push --tags
```

This will start the gitlab pipeline which will deploy the website to https://www.silex.me/