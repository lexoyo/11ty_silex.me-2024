// silex/client.js
import Eleventy from './js/silex-cms/client.js'

export default function(config, options) {
  config.addPlugin(Eleventy, {
    enable11ty: true,
    dataSources: [{
      id: 'squidex',
      type: 'graphql',
      name: 'Squidex',
      url: 'https://cloud.squidex.io/api/content/silex-me/graphql',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjlTZWo3eXJYdGo1TzNDVV9JeG5EU2ciLCJ0eXAiOiJhdCtqd3QifQ.eyJzdWIiOiJzaWxleC1tZTpzaWxleDExdHkiLCJvaV9wcnN0Ijoic2lsZXgtbWU6c2lsZXgxMXR5IiwiY2xpZW50X2lkIjoic2lsZXgtbWU6c2lsZXgxMXR5Iiwib2lfdGtuX2lkIjoiOWE5YjdmNzEtZDJjOC00NjJjLWE2MWUtMTMzMTJiZjBmODQxIiwiYXVkIjoic2NwOnNxdWlkZXgtYXBpIiwic2NvcGUiOiJzcXVpZGV4LWFwaSIsImp0aSI6IjljNWYxM2ZjLWJlYzktNGE5YS04MTJiLTY2YWI5ZmI4MzYyMiIsImV4cCI6MTcwNjQzOTA4MCwiaXNzIjoiaHR0cHM6Ly9jbG91ZC5zcXVpZGV4LmlvLyIsImlhdCI6MTcwMzg0NzA4MH0.VqUUgNjmFb1J6gStqvT0uB0zs3VrpP92_7-S6bKafOX_-BdFEZPw1R2YKH5PuB-X8MXiSNxRpjPsRLRg-6JJ5J2F_5hoIOW8dZkNtwA3MEy8j2tdwAWorPDs4HRkpISQSPCBYiz3L5gnipMMEkpIT_5nfsaztDd6yK0HY28RwfIACdmp9PlTQ9ZQaCyIkPxZh6yAGCpcF2d-2KDxxY0vZ7YFrV4eMzqUxVhvQ07ouGzE15y-K3cfO2I3nmQg5ULQGXmpI4SoXtAptIpLwSjeFJzh2KczQ7XQQU04EO7jh7ceoUjhxvvxhurE_OTh3d3ovAxIPdfvJhv_29wCfTAIiQ`,
        'X-Languages': '${data.lang}',
      },
    }],
    imagePlugin: false,
    i18nPlugin: true,
    fetchPlugin: false,
  })
}