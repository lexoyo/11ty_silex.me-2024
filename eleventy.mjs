// .eleventy.js
import { EleventyI18nPlugin } from '@11ty/eleventy';
import Image from '@11ty/eleventy-img';
import { eleventyImageTransformPlugin } from '@11ty/eleventy-img';

export default function (eleventyConfig) {
  // Serve CSS and assets along with the site
  eleventyConfig.addPassthroughCopy({'published/css/*.css': 'css'});
  eleventyConfig.addPassthroughCopy({'published/assets/': 'assets'});

  // For the fetch plugin
  eleventyConfig.watchIgnores.add('**/.cache/**')

  // i18n plugin
  eleventyConfig.addPlugin(EleventyI18nPlugin, {
    // any valid BCP 47-compatible language tag is supported
    defaultLanguage: 'en',
    errorMode: 'allow-fallback',
  });

  eleventyConfig.addPlugin(eleventyImageTransformPlugin, {
    extensions: 'html',
    formats: ['avif', 'auto'],
    //widths,
    svgShortCircuit: true, // Skip SVG optimization (requires "svg" in formats array)
    defaultAttributes: {
      loading: 'lazy',
      decoding: 'async',
      //sizes: ['100vw'],
      alt: '',
    },
  });

  // Image plugin
  eleventyConfig.addShortcode('image', async function(src, alt, sizes) {
    let metadata = await Image(src, {
      widths: [300, 600],
      formats: ['avif', 'jpeg']
    });
    let imageAttributes = {
      alt,
      sizes,
      loading: 'lazy',
      decoding: 'async',
    };

    // You bet we throw an error on a missing alt (alt='' works okay)
    return Image.generateHTML(metadata, imageAttributes);
  });
};
